/*
 * @Author: Penk
 * @LastEditors: Penk
 * @LastEditTime: 2022-07-14 11:26:46
 * @FilePath: \puppeteer-demo\index.js
 */

// const puppeteer = require('puppeteer');


import puppeteer from 'puppeteer';
import chalk from 'chalk';
import $Fn from './scripts/main.js';
import path from 'path';
import fs from 'fs';

const __dirname = path.resolve();
const iPhone = puppeteer.devices['iPhone 6'];

const log = (...args) => {
  console.log(...args)
};
const error = chalk.bold.red;
const blue = chalk.bold.blue;
const warning = chalk.hex('#FFA500'); // Orange color

const CRX_PATH = path.join(__dirname, 'plugin1');

// log(`
// CPU: ${chalk.red('90%')}
// RAM: ${chalk.green('40%')}
// DISK: ${chalk.yellow('70%')}
// `);

(async () => {
  // await autoTi();
  await taobaoAutoLogin();
})();

async function taobaoAutoLogin() {
  var userDataPath = path.join(__dirname, 'userDataDir');
  if (!fs.existsSync(userDataPath)) {
    fs.mkdirSync(userDataPath);
  }

  const browser = await puppeteer.launch({
    headless: false,
    devtools: true,
    slowMo: 80,
    timeout: 60000,
    // 是否将浏览器进程标准输出和标准错误输入到 process.stdout 和 process.stderr 中。默认是 false。
    // dumpio:true
    defaultViewport: {
      width: 1624,
      height: 768
    },
    // defaultViewport: null,
    userDataDir: './userDataDir',
    // 加载扩展程序
    args: [
      '--start-maximized',
      `--disable-extensions-except=${CRX_PATH}`,
      `--load-extension=${CRX_PATH}`,
      '--user-agent=Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
    ]
  });
  // const page = await browser.newPage();
  const page = (await browser.pages())[0];

  // 这一步很关键 用于屏蔽检测是否显示验证码
  await page.evaluate(async () => {
    Object.defineProperty(navigator, 'webdriver', {
      get: () => false
    })
  });

  // 等待页面超过90s就报错
  page.setDefaultNavigationTimeout(90000);

  log(blue(await browser.userAgent()));


  $Fn.taobaoAutoLogin({
    browser,
    page
  });
}

async function autoTi() {
  var userDataPath = path.join(__dirname, 'userDataDir');
  if (!fs.existsSync(userDataPath)) {
    fs.mkdirSync(userDataPath);
  }

  const browser = await puppeteer.launch({
    headless: false,
    devtools: true,
    slowMo: 80,
    timeout: 60000,
    // 是否将浏览器进程标准输出和标准错误输入到 process.stdout 和 process.stderr 中。默认是 false。
    // dumpio:true
    defaultViewport: {
      width: 1624,
      height: 768
    },
    // defaultViewport: null,
    userDataDir: './userDataDir',
    // 加载扩展程序
    args: [
      '--start-maximized',
      `--disable-extensions-except=${CRX_PATH}`,
      `--load-extension=${CRX_PATH}`,
      '--user-agent=Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
    ]
  });
  // const page = await browser.newPage();
  const page = (await browser.pages())[0];
  // 等待页面超过90s就报错
  page.setDefaultNavigationTimeout(90000);

  log(blue(await browser.userAgent()));

  $Fn.autoTi({
    browser,
    page
  });
}